package com.braithewarnock.practicemultithreading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeMultithreadingApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticeMultithreadingApplication.class, args);
	}

}
