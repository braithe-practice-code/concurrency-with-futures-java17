package com.braithewarnock.practicemultithreading.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MangoTreeController {

    @GetMapping("/")
    public String GetMangoTree() {
        return "Kesar mango tree!";
    }

}
